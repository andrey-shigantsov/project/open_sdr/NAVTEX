/*!
 * \file Transmitter.c
 * \author Андрей Шиганцов
 * \brief
 */

#include "Transmitter.h"

static Bool_t next_bit(NAVTEX_Transmitter_t* This, Bit_t* bit)
{
  if (This->SymCounter == 0)
  {
    if ( (This->on_buffer_ready)
         &&(RingBufferInt8_UsedSpace(&This->Buffer)/2 <= This->BufferReadyTriggerLevel)
         &&(RingBufferInt8_UsedSpace(&This->Buffer)%2 == 0) )
      This->on_buffer_ready(This->Parent);

    if (!RingBufferInt8_read_1(&This->Buffer, &This->CurrentByte))
    {
      if (This->on_transmit_finished)
        This->on_transmit_finished(This->Parent);
      return 0;
    }
  }
  *bit = (This->CurrentByte >> This->SymCounter++) & 1;
  if (This->SymCounter == NAVTEX_CODE_SIZE)
    This->SymCounter = 0;
  return 1;
}

static void fill_empty_buf(NAVTEX_Transmitter_t* This, iqSample_t* Samples, Size_t Count)
{
  SDR_UNUSED(This);
  clear_buffer_iq(Samples, Count);
}

void init_NAVTEX_Transmitter(NAVTEX_Transmitter_t* This, NAVTEX_TransmitterCfg_t* cfg)
{
  This->Parent = 0;
  This->on_buffer_ready = This->on_transmit_finished = 0;

  init_NAVTEX_Encoder(&This->Encoder);

  init_RingBufferInt8(&This->Buffer, &cfg->Buffer);
  This->BufferReadyTriggerLevel = cfg->BufferReadyTriggerLevel;

  FSK_ModulatorCfg_t cfgModulator;
  cfgModulator.Fd = cfg->Fd;
  cfgModulator.GeneratorBuf = cfg->generatorBuf;
  cfgModulator.Freq0 = SDR_NAVTEX_FREQUENCY_SPACING/2;
  cfgModulator.Freq1 = -SDR_NAVTEX_FREQUENCY_SPACING/2;
  cfgModulator.BaudRate = SDR_NAVTEX_SYMBOLS_RATE;
  init_FSK_Modulator(&This->Modulator, &cfgModulator);

  FSK_ModulatorCallback_t callbackModulator;
  callbackModulator.next_bit = (get_bit_t)next_bit;
  callbackModulator.fill_empty_buf = (samples_sink_iq_t)fill_empty_buf;
  FSK_Modulator_set_callback(&This->Modulator, This, &callbackModulator);

  This->SymCounter = 0;
}
