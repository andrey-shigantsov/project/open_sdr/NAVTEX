/*!
 * \file ita2.c
 * \author Андрей Шиганцов
 * \brief
 */

/*!
 * \file ita2.c
 * \author Андрей Шиганцов
 * \brief Исходники международноо телеграфного кода
 */

#include "ita2.h"

Int8_t ita2_encode(Int8_t Char)
{
  int i;
  i = ita2_ascii_index(Char);
  if (i == -1) return ITA2_ERROR;

  return ITA2_Table[i];
}

Int8_t ita2_decode(Int8_t ita2Char, Bool_t isNumeric)
{
  SDR_ASSERT((isNumeric == 0)||(isNumeric == 1));
  int i;
  i = ita2_index(ita2Char);
  if (i == -1) return ITA2_ERROR;

  return ITA2_ASCII_Table[i][isNumeric];
}

Int8_t ita2_index(Int8_t ita2Char)
{
  int i;
  for (i = 0; i < ITA2_CHARACTERS_COUNT; i++)
    if (ita2Char == ITA2_Table[i]) return i;

  return -1;
}

Int8_t ita2_ascii_index(Int8_t asciiChar)
{
  int i;
  for (i = 0; i < ITA2_CHARACTERS_COUNT; i++)
    if ((asciiChar == ITA2_ASCII_Table[i][0])||(asciiChar == ITA2_ASCII_Table[i][1]))
      return i;

  return -1;
}

Bool_t ita2_switch_service(Int8_t asciiChar)
{
  int i;
  for (i = ITA2_SERVICE_CHARS_INDEX; i < ITA2_CHARACTERS_COUNT; i++)
    if (asciiChar == ITA2_ASCII_Table[i][0])
      return 1;
  return 0;
}

Bool_t ita2_switch_alphabetic(Int8_t asciiChar)
{
  int i;
  for (i = 0; i < ITA2_SERVICE_CHARS_INDEX; i++)
    if (asciiChar == ITA2_ASCII_Table[i][0])
      return 1;
  return 0;
}

Bool_t ita2_switch_numeric(Int8_t asciiChar)
{
  int i;
  for (i = 0; i < ITA2_SERVICE_CHARS_INDEX; i++)
    if (asciiChar == ITA2_ASCII_Table[i][1])
      return 1;
  return 0;
}

const Int8_t ITA2_Table[ITA2_CHARACTERS_COUNT] =
{
  //	Значение  |	 позиции битов
  //			  |	 1 2 3 4 5
  //--------------------------------
  0x03,	// ZZAAA
  0x19,	// ZAAZZ
  0x0E,	// AZZZA
  0x9,	// ZAAZA
  0x1, 	// ZAAAA
  0x0D, 	// ZAZZA
  0x1A, 	// AZAZZ
  0x14, 	// AAZAZ
  0x06, 	// AZZAA
  0x0B, 	// ZZAZA
  0x0F, 	// ZZZZA
  0x12, 	// AZAAZ
  0x1C, 	// AAZZZ
  0x0C, 	// AAZZA
  0x18, 	// AAAZZ
  0x16, 	// AZZAZ
  0x17, 	// ZZZAZ
  0x0A, 	// AZAZA
  0x05, 	// ZAZAA
  0x10, 	// AAAAZ
  0x07, 	// ZZZAA
  0x1E, 	// AZZZZ
  0x13, 	// ZZAAZ
  0x1D, 	// ZAZZZ
  0x15, 	// ZAZAZ
  0x11, 	// ZAAAZ
  0x08, 	// AAAZA
  0x02, 	// AZAAA
  0x1F, 	// ZZZZZ
  0x1B, 	// ZZAZZ
  0x04, 	// AAZAA
  0x00  	// AAAAA
};

const Int8_t ITA2_ASCII_Table[ITA2_CHARACTERS_COUNT][2] =
{
  {'A', '-'},
  {'B', '?'},
  {'C', ':'},
  {'D', ITA2_ASCII_MALTIYSKY_CROSS},
  {'E', '3'},
  {'F', ITA2_ASCII_RESERVED},
  {'G', ITA2_ASCII_RESERVED},
  {'H', ITA2_ASCII_RESERVED},
  {'I', '8'},
  {'J', ITA2_ASCII_SOUND},
  {'K', '('},
  {'L', ')'},
  {'M', '.'},
  {'N', ','},
  {'O', '9'},
  {'P', '0'},
  {'Q', '1'},
  {'R', '4'},
  {'S', '\''},
  {'T', '5'},
  {'U', '7'},
  {'V', '='},
  {'W', '2'},
  {'X', '/'},
  {'Y', '6'},
  {'Z', '+'},

  {'\r', '\r'},
  {'\n', '\n'},
  {ITA2_ASCII_ALPHABETIC, ITA2_ASCII_ALPHABETIC},
  {ITA2_ASCII_NUMERIC, ITA2_ASCII_NUMERIC},
  {' ', ' '},
  {ITA2_ASCII_NO_INF, ITA2_ASCII_NO_INF}
};


