/*!
 * \file Decoder.h
 * \author Андрей Шиганцов
 * \brief
 */

#ifndef DECODER_H_
#define DECODER_H_

#include "SDR/BASE/common.h"
#include "code.h"
#include "SDR/BASE/Abstract.h"

#ifdef __cplusplus
extern "C"
{
#endif

typedef enum
{
  navtex_ERROR_NULL, navtex_ERROR_1CH, navtex_ERROR_2CH
} NAVTEX_Reliability_t;

/*! Структура декодированного символа НАВТЕКС */
typedef struct
{
  Int8_t Data; 		/*!< Декодированный символ */
  NAVTEX_Reliability_t Reliability;	/*!< Достоверность декодированного символа */
} NAVTEX_DecoderChar_t;

typedef struct
{
  Int8_t AlphaCountForEnd;
} NAVTEX_DecoderCfg_t;

typedef void (*NAVTEX_Decoder_char_sink)(void* This, NAVTEX_DecoderChar_t* Char);

typedef struct
{
  event_sink_t on_start;
  event_sink_t on_end;
  NAVTEX_Decoder_char_sink on_char;
} NAVTEX_DecoderCallback_t;

/*! Структура данных декодера */
typedef struct
{
  void* Parent;

  Int8_t DX[3],		/*!< Буфер временного канала DX */
  RX[3];			/*!< Буфер временного канала RX */
  Bool_t isSync;		/*!< Флаг обнаружения синхропоследовательности */
  Int8_t endCounter;	/*!< Счётчик завершающих символов */
  Int8_t endCount;

  event_sink_t on_start;
  event_sink_t on_end;
  NAVTEX_Decoder_char_sink on_char;
} NAVTEX_Decoder_t;

INLINE void NAVTEX_Decoder_set_callback(NAVTEX_Decoder_t* This, void* Parent, NAVTEX_DecoderCallback_t* Callback)
{
  This->Parent = Parent;
  This->on_start = Callback->on_start;
  This->on_end = Callback->on_end;
  This->on_char = Callback->on_char;
}

INLINE void NAVTEX_Decoder_reset(NAVTEX_Decoder_t* This)
{
  This->isSync = 0;
  This->endCounter = 0;
}

/*! Инициализация НАВТЕКС декодера */
void init_NAVTEX_Decoder(NAVTEX_Decoder_t *This, NAVTEX_DecoderCfg_t* cfg);

/*! НАВТЕКС декодер */
void navtex_decoder_1(NAVTEX_Decoder_t* This,
                      NAVTEX_Code_t Code /*!< [in] теукщий 2-х канальный(dx и rx) код */);

INLINE void navtex_decoder (NAVTEX_Decoder_t *This,	NAVTEX_Code_t* Codes, Size_t Count)
{
  Size_t i;
  for (i = 0; i < Count; i++)
  {
    navtex_decoder_1(This, Codes[i]);
  }
}

#ifdef __cplusplus
}
#endif

#endif /* DECODER_H_ */
