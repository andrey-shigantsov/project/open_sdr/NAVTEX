/*!
 * \file Encoder.h
 * \author Андрей Шиганцов
 * \brief
 */

#ifndef ENCODER_H_
#define ENCODER_H_

#include "SDR/BASE/common.h"
#include "code.h"

#ifdef __cplusplus
extern "C"
{
#endif

/*! Кодер НАВТЕКС */
typedef struct
{
  int16_t RX[2];
} NAVTEX_Encoder_t;

/*! Инициализация кодера НАВТЕКС */
void init_NAVTEX_Encoder(NAVTEX_Encoder_t* This);

/*! Кодер НАВТЕКС */
void navtex_encoder_1(NAVTEX_Encoder_t* This,
                      Int8_t Char, /*!< [in]  Служебный или ITA2 символ */
                      NAVTEX_Code_t Code /*!< [out] теукщий 2-х канальный(dx и rx) код */);
INLINE void navtex_encoder(NAVTEX_Encoder_t* This,
                           Int8_t* Chars, Size_t Count, NAVTEX_Code_t* Codes)
{
  Size_t i;
  for (i = 0 ; i < Count; i++)
    navtex_encoder_1(This, Chars[i], Codes[i]);
}

#ifdef __cplusplus
}
#endif

#endif /* ENCODER_H_ */
